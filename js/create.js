const requeridos = [
    "ApellidoPaterno",
    "ApellidoMaterno",
    "Nombre",
    "Sexo",
    "FechaNacimiento",
    "NumeroEmpleado",
    "CURP",
    "RFC",
    "EstadoCivil",
    "TipoSangre",
    "Estatura",
    "Peso",
    "Complexion",
    "Pais",
    "Estado",
    "Municipio",
    "Localidad",
    "Colonia",
    "CodigoPostal",
    "TipoVialidad",
    "NombreVialidad",
    "NumeroExterior"
]

const no_requeridos = [
    "NumeroPension",
    "Fotografia",
    "Discapacidad",
    "NumeroInterior"
]

const requerido_estudios = [
    "Escuela",
    "Grado",
    "FechaInicio",
    "FechaFin"
]

const campos_direccion = [
    "Pais",
    "Estado",
    "Municipio",
    "Localidad",
    "Colonia",
]

let estudios = []
let Fotografia = null


let validar_requeridos = (req, vals = true) => {
    let error = false
    let valores = {}
    req.forEach(r => {
        let item = $("#" + r)
        console.log(item)
        let val
        try{
            val = item ? item[0].value ? item[0].value.toString().trim() : null : null
        }catch(e){
            val = null
        }
        if(vals) valores[r] = val
        console.log(val)
        if (val) {
            item.parent().removeClass("has-error")
            item.parent().addClass("has-success")
        } else {
            error = true
            item.parent().removeClass("has-success")
            item.parent().addClass("has-error")
        }
    });
    return { error, valores }
}

let fill_object = () => {
    let o = {}
    requeridos.forEach(r => {
        let item = $("#" + r)
        let val = item ? item[0].value ? item[0].value.toString().trim() : null : null
        o[r] = val
    });
    no_requeridos.forEach(r => {
        let item = $("#" + r)
        let val = item ? item[0].value ? item[0].value.toString().trim() : null : null
        o[r] = val
    });
    o.Estudios = estudios
    o.Fotografia = Fotografia
    return o
}

let request = (data) => {
    return new Promise((resolve, reject) => {
        $.ajax({
            url: "/",
            method: "POST",
            data,
            success: (data) => {
                //data = JSON.parse(data)
                return resolve(data)
            },
            error: (e) => {
                return reject(e)
            }
        })
    })

}

let update_table = () => {
    $('#estudios').find('tbody>tr').remove()
    estudios.forEach((e, i) => {
        $('#estudios').append($('<tr>').append($('<td>').text(e.Escuela))
            .append($('<td>').text(e.Grado))
            .append($('<td>').text(e.FechaInicio))
            .append($('<td>').text(e.FechaFin))
            .append($('<td>').append($('<button class="btn btn-danger eliminar" data-id="' + i + '">').text('X').click((e) => {
                console.log($(e.target).attr('data-id'))
                let x = $(e.target).attr('data-id')
                estudios = estudios.filter((e, i) => i != x)
                console.log(estudios)
                update_table()
            }))))
    })
}

let clean_estudios = () => {
    requerido_estudios.forEach(r => {
        $("#" + r)[0].value = ""
    })
}

let readURL = (input) => {
    if (input.files && input.files[0]) {
        var reader = new FileReader()
        reader.onload = function (e) {
            $('#img').attr('src', e.target.result)
            Fotografia = e.target.result
        }
        reader.readAsDataURL(input.files[0]);
    }
}



$(() => {
    let id = $("#id")[0].value
    console.log(id)
    $(".set_select2").select2()
    $("#CURP").mask("AAAA000000AAAAAA00")
    $("#RFC").mask("AAAA00000AAA")
    $(".set_select2").change((e) => {
        let item = $(e.target)[0]
        let i = campos_direccion.findIndex(c => c == item.name)
        if (i > -1 && i + 1 < campos_direccion.length) {
            let next = campos_direccion[i + 1]
            let data = {
                model: "catalogos",
                action: next,
                ids: campos_direccion.filter((c, j) => j < i + 1).map(c => $('#' + c)[0].value)
            }
            $('#' + next).find('option').remove()
            request(data).then(res => {
                res.forEach(r => {
                    $('#' + next).append($(`<option>`).prop('value', r.Id).text(r.Descripcion))
                })
                $('#' + next).change()
            }).catch(e => {
                console.log(e)
            })
        }
    })
    if(!id){
        $('#Pais').change()
    }
    $('#agregar').click((e) => {
        let data = validar_requeridos(requerido_estudios)
        if (!data.error) {
            estudios.push(data.valores)
            console.log(estudios)
            update_table()
            clean_estudios()
        }
    })
    $("#Fotografia").change((e) => {
        readURL(e.target);
    });
    $('#guardar').click(e => {
        let data = validar_requeridos(requeridos, false)
        if(!data.error){
            let datos = fill_object()
            datos.id = id 
            request({
                model: "Empleado",
                action: "Crear",
                data: datos  
            }).then(r => {
                if(r.resultado){
                    window.location = "?" 
                }else{
                    alert('fallo el guardado del empleado')
                }
            }).catch(e => {
                alert('fallo el guardado del empleado')
            })
        }
    })

})

