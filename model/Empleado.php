<?php 
require_once "Estudio.php";

class Empleado{

    public $Id;
    public $ApellidoPaterno;
    public $ApellidoMaterno;
    public $Nombre;
    public $Sexo;
    public $FechaNacimiento;
    public $NumeroEmpleado;
    public $CURP;
    public $RFC;
    public $EstadoCivil;
    public $TipoSangre;
    public $Estatura;
    public $Peso;
    public $Complexion;
    public $Pais;
    public $Estado;
    public $Municipio;
    public $Localidad;
    public $Colonia;
    public $CodigoPostal;
    public $TipoVialidad;
    public $NombreVialidad;
    Public $NumeroExterior;
    public $NumeroPension;
    public $Fotografia;
    public $Discapacidad;
    public $NumeroInterior;
    public $Estudios;

    private $path;

    public function Empleado(){
        $this->path = "bd/";
        $this->img = "img/"; 
    }

    public function guardar($data){
        // var_dump($data['id']);
        $this->Id = isset($data['id']) & $data['id'] != "" ? $data['id'] : $this->getId();
        // var_dump($this->Id);
        $this->set($data);
        $errores = $this->validar();
        if(count($errores) > 0){
            return false;
        }else{
            $str = explode(',', $this->Fotografia);
            $type = substr($str[0], strpos($str[0], '/') + 1,strpos($str[0], ';') - strpos($str[0], '/') - 1);
            if($this->fotografia()){
                $this->Fotografia = $this->Id . "." .$type;
            }
            $this->estudios();
            $this->crear_archivo((isset($data['id']) && $data['id'] != "" ) ? $data['id'] : null);
            return true;
        }

    }   
    
    public function set($data){
        $this->ApellidoPaterno = isset($data["ApellidoPaterno"]) ? $data["ApellidoPaterno"] : null;
        $this->ApellidoMaterno = isset($data["ApellidoMaterno"]) ? $data["ApellidoMaterno"] : null;
        $this->Nombre = isset($data["Nombre"]) ? $data["Nombre"] : null;
        $this->Sexo = isset($data["Sexo"]) ? $data["Sexo"] : null;
        $this->FechaNacimiento = isset($data["FechaNacimiento"]) ? $data["FechaNacimiento"] : null;
        $this->NumeroEmpleado = isset($data["NumeroEmpleado"]) ? $data["NumeroEmpleado"] : null;
        $this->CURP = isset($data["CURP"]) ? $data["CURP"] : null;
        $this->RFC = isset($data["RFC"]) ? $data["RFC"] : null;
        $this->EstadoCivil = isset($data["EstadoCivil"]) ? $data["EstadoCivil"] : null;
        $this->TipoSangre = isset($data["TipoSangre"]) ? $data["TipoSangre"] : null;
        $this->Estatura = isset($data["Estatura"]) ? $data["Estatura"] : null;
        $this->Peso = isset($data["Peso"]) ? $data["Peso"] : null;
        $this->Complexion = isset($data["Complexion"]) ? $data["Complexion"] : null;
        $this->Pais = isset($data["Pais"]) ? $data["Pais"] : null;
        $this->Estado = isset($data["Estado"]) ? $data["Estado"] : null;
        $this->Municipio = isset($data["Municipio"]) ? $data["Municipio"] : null;
        $this->Localidad = isset($data["Localidad"]) ? $data["Localidad"] : null;
        $this->Colonia = isset($data["Colonia"]) ? $data["Colonia"] : null;
        $this->CodigoPostal = isset($data["CodigoPostal"]) ? $data["CodigoPostal"] : null;
        $this->TipoVialidad = isset($data["TipoVialidad"]) ? $data["TipoVialidad"] : null;
        $this->NombreVialidad = isset($data["NombreVialidad"]) ? $data["NombreVialidad"] : null;
        $this->NumeroExterior = isset($data["NumeroExterior"]) ? $data["NumeroExterior"] : null;
        $this->NumeroPension = isset($data["NumeroPension"]) ? $data["NumeroPension"] : null;
        $this->Fotografia = isset($data["Fotografia"]) ? $data["Fotografia"] : null;
        $this->Discapacidad = isset($data["Discapacidad"]) ? $data["Discapacidad"] : null;
        $this->NumeroInterior = isset($data["NumeroInterior"]) ? $data["NumeroInterior"] : null;
        $this->Estudios = isset($data["Estudios"]) ? $data["Estudios"] : null;
    }

    public function estudios(){
        $tmp = array_map(function($e) {
            $estudio = new Estudio();
            return $estudio->set($e);
        }, $this->Estudios ? $this->Estudios : []);
    }

    public function fotografia(){
        if($this->Fotografia){
            $str = explode(',', $this->Fotografia);
            $type = substr($str[0], strpos($str[0], '/') + 1,strpos($str[0], ';') - strpos($str[0], '/') - 1);
            $data = base64_decode($str[1]);
            $file = fopen($this->img . $this->Id . "." . $type, "wb");
            fwrite($file, $data);
            fclose($file);
            return true;
        }
        return false;
    }

    private function crear_archivo($id){
        // var_dump($id);
        if($id){
            $tmp = $this->buscar($id);
            if($tmp){
                $this->Fotografia = $this->Fotografia ? $this->Fotografia : $tmp->Fotografia;
                unlink($this->path . $id . ".txt");
            }
        }
        // var_dump(($id ? $id : $this->Id));
        $text = json_encode($this);
        $file = fopen($this->path . ($id ? $id : $this->Id) . ".txt", "w");
        fwrite($file, $text);
        fclose($file);
    }

    public function listar(){
        $files = scandir($this->path);
        $empleados = [];
        foreach($files as $file){
            $content = json_decode(file_get_contents($this->path . $file));
            if($content != null){
                $empleados[] = $content;
            }
        }
        return $empleados;
    }

    public function buscar($id){
        try{
            return json_decode(file_get_contents($this->path . $id . ".txt"));
        }catch(Exception $e){
            return null;
        }
    }

    public function getId(){
        return uniqid();
    }
    
    public function validar(){
        $error = [];
        if($this->ApellidoPaterno == "" && empty($this->ApellidoPaterno)){
            $error[] = "El campor ApellidoPaterno esta vacio";
        }
        if($this->ApellidoMaterno == "" && empty($this->ApellidoMaterno)){
            $error[] = "El campor ApellidoMaterno esta vacio";
        }
        if($this->Nombre == "" && empty($this->Nombre)){
            $error[] = "El campor Nombre esta vacio";
        }
        if($this->Sexo == "" && empty($this->Sexo)){
            $error[] = "El campor Sexo esta vacio";
        }
        if($this->FechaNacimiento == "" && empty($this->FechaNacimiento)){
            $error[] = "El campor FechaNacimiento esta vacio";
        }
        if($this->NumeroEmpleado == "" && empty($this->NumeroEmpleado)){
            $error[] = "El campor NumeroEmpleado esta vacio";
        }
        if($this->CURP == "" && empty($this->CURP)){
            $error[] = "El campor CURP esta vacio";
        }
        if($this->RFC == "" && empty($this->RFC)){
            $error[] = "El campor RFC esta vacio";
        }
        if($this->EstadoCivil == "" && empty($this->EstadoCivil)){
            $error[] = "El campor EstadoCivil esta vacio";
        }
        if($this->TipoSangre == "" && empty($this->TipoSangre)){
            $error[] = "El campor TipoSangre esta vacio";
        }
        if($this->Estatura == "" && empty($this->Estatura)){
            $error[] = "El campor Estatura esta vacio";
        }
        if($this->Peso == "" && empty($this->Peso)){
            $error[] = "El campor Peso esta vacio";
        }
        if($this->Complexion == "" && empty($this->Complexion)){
            $error[] = "El campor Complexion esta vacio";
        }
        if($this->Pais == "" && empty($this->Pais)){
            $error[] = "El campor Pais esta vacio";
        }
        if($this->Estado == "" && empty($this->Estado)){
            $error[] = "El campor Estado esta vacio";
        }
        if($this->Municipio == "" && empty($this->Municipio)){
            $error[] = "El campor Municipio esta vacio";
        }
        if($this->Localidad == "" && empty($this->Localidad)){
            $error[] = "El campor Localidad esta vacio";
        }
        if($this->Colonia == "" && empty($this->Colonia)){
            $error[] = "El campor Colonia esta vacio";
        }
        if($this->CodigoPostal == "" && empty($this->CodigoPostal)){
            $error[] = "El campor CodigoPostal esta vacio";
        }
        if($this->TipoVialidad == "" && empty($this->TipoVialidad)){
            $error[] = "El campor TipoVialidad esta vacio";
        }
        if($this->NombreVialidad == "" && empty($this->NombreVialidad)){
            $error[] = "El campor NombreVialidad esta vacio";
        }
        if($this->NumeroExterior == "" && empty($this->NumeroExterior)){
            $error[] = "El campor NumeroExterior esta vacio";
        }

        return $error;
    }

}