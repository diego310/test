<?php

class Estudio{

    Public $Escuela;
    Public $Grado;
    Public $FechaInicio;
    Public $FechaFin;

    public function set($data){
        $this->Escuela = isset($data["Escuela"]) ? $data["Escuela"] : "";
        $this->Grado = isset($data["Grado"]) ? $data["Grado"] : "";
        $this->FechaInicio = isset($data["FechaInicio"]) ? $data["FechaInicio"] : "";
        $this->FechaFin = isset($data["FechaFin"]) ? $data["FechaFin"] : "";
        if($this->validar()){
            return $this;
        }
        return null;
    }

    public function validar(){
        if($this->Escuela == "" || empty($this->Escuela)){
            return false;
        } 
        if($this->Grado == "" || empty($this->Grado)){
            return false;
        } 
        if($this->FechaInicio == "" || empty($this->FechaInicio)){
            return false;
        } 
        if($this->FechaFin == "" || empty($this->FechaFin)){
            return false;
        } 
        return true;
    }
}