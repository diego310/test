<?php
error_reporting(0);
require_once "model/Catalogo.php";
require_once "model/Empleado.php";
if(isset($_GET['page'])){
    $page = $_GET['page'];
    include "view/head.html";
    switch($page){
        case "home": 
            $empleado = new Empleado();
            $empleados = $empleado->listar();
            include "view/home.php";
            
        break;
        case "crear": 
            $catalogo = new Catalogo(); 
            $empleado = new Empleado();
            $id = isset($_GET["Id"]) ? $_GET["Id"] : null ;
            $empleadoObj = null ;
            if($id){
                $empleadoObj = $empleado->buscar($id);
                $estados = array_filter($catalogo->GetCatEstado(), function($estado) use ($empleadoObj) {return $estado['IdCatPais'] == $empleadoObj->Pais;});
                $municipios = array_filter($catalogo->GetCatMunicipio(), function($estado) use ($empleadoObj) {return $estado['IdCatPais'] == $empleadoObj->Pais && $estado['IdCatEstado'] == $empleadoObj->Estado;});
                $localidades = array_filter($catalogo->GetCatLocalidad(), function($estado) use ($empleadoObj) {return $estado['IdCatPais'] == $empleadoObj->Pais && $estado['IdCatEstado'] == $empleadoObj->Estado && $estado['IdCatMunicipio'] == $empleadoObj->Municipio;});
                $colonias = array_filter($catalogo->GetCatColonia(), function($estado) use ($empleadoObj) {return $estado['IdCatPais'] == $empleadoObj->Pais && $estado['IdCatEstado'] == $empleadoObj->Estado && $estado['IdCatMunicipio'] == $empleadoObj->Municipio && $estado['IdCatLocalidad'] == $empleadoObj->Localidad;});
            }
            $sexos = $catalogo->GetCatSexo();
            $discapacidades = $catalogo->GetCatDsicapacidad();
            $civiles = $catalogo->GetCatEstadoCivil();
            $grados= $catalogo->GetCatGradoEstudio();
            $paises = $catalogo->GetCatPais();
            $sangre = $catalogo->GetCatTipoSangre();
            $complexiones = $catalogo->GetCatComplexion();
            include "view/create.php";
        break;
        default: 
            $empleado = new Empleado();
            $empleados = $empleado->listar();   
            include "view/home.php";
        break;
    }
    include "view/foot.html";   
}else if(isset($_POST["action"]) && isset($_POST["model"])){
    $action = $_POST["action"];
    $model = $_POST["model"];
    header('Content-Type: application/json');
    switch($model){
        case "catalogos": 
            $ids = $_POST["ids"];
            //var_dump($ids);
            $catalogo = new Catalogo();
            switch($action){
                case "Estado": 
                    echo json_encode(array_values(array_filter($catalogo->GetCatEstado(), function($estado) use ($ids) {return $estado['IdCatPais'] == $ids[0]; })));
                break;
                case "Municipio": 
                    echo json_encode(array_values(array_filter($catalogo->GetCatMunicipio(), function($estado) use ($ids) {return $estado['IdCatPais'] == $ids[0] && $estado['IdCatEstado'] == $ids[1];})));
                break;
                case "Localidad": 
                    echo json_encode(array_values(array_filter($catalogo->GetCatLocalidad(), function($estado) use ($ids) {return $estado['IdCatPais'] == $ids[0] && $estado['IdCatEstado'] == $ids[1] && $estado['IdCatMunicipio'] == $ids[2];})));
                break;
                case "Colonia": 
                    echo json_encode(array_values(array_filter($catalogo->GetCatColonia(), function($estado) use ($ids) {return $estado['IdCatPais'] == $ids[0] && $estado['IdCatEstado'] == $ids[1] && $estado['IdCatMunicipio'] == $ids[2] && $estado['IdCatLocalidad'] == $ids[3];})));
                break;
                default:
                    echo json_encode([]);
                break;
            }
        break;
        case "Empleado": 
            $data = $_POST["data"];
            //var_dump($data);
            $empleado = new Empleado();
            switch($action){
                case "Crear": 
                    echo json_encode(["resultado" => $empleado->guardar($data)]);
                break;
            }
        break;
    }   
    exit;
}else {
    include "view/head.html";
    $empleado = new Empleado();
    $empleados = $empleado->listar();   
    include "view/home.php";
    include "view/foot.html";
}

