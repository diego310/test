<div class="container">
    <div class="row">
        <div class="col-md-4">
            <input type="hidden" id="id" value="<?=$id?>">
            <legend>Datos Generales</legend>
            <div class="form-group">
                <label>ApellidoPaterno*</label>
                <input class="form-control" type="text" id="ApellidoPaterno" value="<?=$empleadoObj ? $empleadoObj->ApellidoPaterno : "" ?>" name="ApellidoPaterno" />
            </div>
            <div class="form-group">
                <label>ApellidoMaterno*</label>
                <input class="form-control" type="text" id="ApellidoMaterno" value="<?=$empleadoObj ? $empleadoObj->ApellidoMaterno : "" ?>" name="ApellidoMaterno" />
            </div>
            <div class="form-group">
                <label>Nombre*</label>
                <input class="form-control" type="text" id="Nombre" value="<?=$empleadoObj ? $empleadoObj->Nombre : "" ?>" name="Nombre" />
            </div>
            <div class="form-group">
                <label>Sexo*</label>
                <select class="form-control set_select2" id="Sexo" name="Sexo">
                    <?php foreach($sexos as $o): ?>
                    <?php echo($o)?>
                    <option value="<?=$o["Id"] ?>">
                        <?=$o["Descripcion"]?>
                    </option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label>FechaNacimiento*</label>
                <input class="form-control" type="date" id="FechaNacimiento" value="<?=$empleadoObj ? $empleadoObj->FechaNacimiento : "" ?>" name="FechaNacimiento" />
            </div>
            <div class="form-group">
                <label>NumeroEmpleado*</label>
                <input class="form-control" type="text" id="NumeroEmpleado" value="<?=$empleadoObj ? $empleadoObj->NumeroEmpleado : "" ?>" name="NumeroEmpleado" />
            </div>
            <div class="form-group">
                <label>NumeroPension</label>
                <input class="form-control" type="text" id="NumeroPension" value="<?=$empleadoObj ? $empleadoObj->NumeroPension : "" ?>" name="NumeroPension" />
            </div>
            <div class="form-group">
                <label>Fotografia</label>
                <input class="form-control" type="file" id="Fotografia" name="Fotografia" accept="image/*" />
                <div class="text-center">
                <img class="col-md-12" src="<?= $id ? "img/" .$empleadoObj->Fotografia : "img/Silueta.png"?>" alt="Imagen no disponible" id="img" style="max-height: 100%">
                    
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <legend>Datos Adicionales</legend>
            <div class="form-group">
                <label>CURP*</label>
                <input class="form-control" type="text" id="CURP" value="<?=$empleadoObj ? $empleadoObj->CURP : "" ?>" name="CURP" />
            </div>
            <div class="form-group">
                <label>RFC*</label>
                <input class="form-control" type="text" id="RFC" value="<?=$empleadoObj ? $empleadoObj->RFC : "" ?>" name="RFC" />
            </div>
            <div class="form-group">
                <label>EstadoCivil*</label>
                <select class="form-control set_select2" id="EstadoCivil" name="EstadoCivil">
                    <?php foreach($civiles as $o): ?>
                    <?php echo($o)?>
                    <option value="<?=$o["Id"] ?>">
                        <?=$o["Descripcion"]?>
                    </option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label>TipoSangre*</label>
                <select class="form-control set_select2" id="TipoSangre" name="TipoSangre">
                    <?php foreach($sangre as $o): ?>
                    <?php echo($o)?>
                    <option value="<?=$o["Id"] ?>">
                        <?=$o["Descripcion"]?>
                    </option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label>Estatura*</label>
                <input class="form-control" type="text" id="Estatura" value="<?=$empleadoObj ? $empleadoObj->Estatura : "" ?>" name="Estatura" />
            </div>
            <div class="form-group">
                <label>Peso*</label>
                <input class="form-control" type="text" id="Peso" value="<?=$empleadoObj ? $empleadoObj->Peso : "" ?>" name="Peso" />
            </div>
            <div class="form-group">
                <label>Complexion*</label>
                <select class="form-control set_select2" id="Complexion" name="Complexion">
                    <?php foreach($complexiones as $o): ?>
                    <?php echo($o)?>
                    <option value="<?=$o["Id"] ?>">
                        <?=$o["Descripcion"]?>
                    </option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label>Discapacidad</label>
                <select class="form-control set_select2" id="Discapacidad" name="Discapacidad">
                    <option value="0">No especificado</option>
                    <?php foreach($discapacidades as $o): ?>
                    <?php echo($o)?>
                    <option value="<?=$o["Id"] ?>">
                        <?=$o["Descripcion"]?>
                    </option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>
        <div class="col-md-4">
            <legend>Domicilio</legend>
            <div class="form-group">
                <label>Pais*</label>
                <select class="form-control set_select2" id="Pais" name="Pais">
                    <?php foreach($paises as $o): ?>
                    <option value="<?=$o["Id"] ?>" <?=$id && $empleadoObj->Pais == $o["Id"]  ? "selected" : ""?>>
                        <?=$o["Descripcion"]?>
                    </option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label>Estado*</label>
                <select class="form-control set_select2" id="Estado" name="Estado">
                        <?php foreach($estados as $o): ?>
                        <option value="<?=$o["Id"] ?>" <?=$id && $empleadoObj->Estado == $o["Id"]  ? "selected" : ""?>>
                            <?=$o["Descripcion"]?>
                        </option>
                        <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label>Municipio*</label>
                <select class="form-control set_select2" id="Municipio" name="Municipio">
                        <?php foreach($municipios as $o): ?>
                        <option value="<?=$o["Id"] ?>" <?=$id && $empleadoObj->Municipio == $o["Id"]  ? "selected" : ""?>>
                            <?=$o["Descripcion"]?>
                        </option>
                        <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label>Localidad*</label>
                <select class="form-control set_select2" id="Localidad" name="Localidad">
                    <?php foreach($localidades as $o): ?>
                    <option value="<?=$o["Id"] ?>" <?=$id && $empleadoObj->Localidad == $o["Id"]  ? "selected" : ""?>>
                        <?=$o["Descripcion"]?>
                    </option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label>Colonia*</label>
                <select class="form-control set_select2" id="Colonia" name="Colonia">
                        <?php foreach($colonias as $o): ?>
                        <option value="<?=$o["Id"] ?>" <?=$id && $empleadoObj->Colonia == $o["Id"]  ? "selected" : ""?>>
                            <?=$o["Descripcion"]?>
                        </option>
                        <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label>CodigoPostal*</label>
                <input class="form-control" type="text" id="CodigoPostal" value="<?=$empleadoObj ? $empleadoObj->CodigoPostal : "" ?>" name="CodigoPostal" />
            </div>
            <div class="form-group">
                <label>TipoVialidad*</label>
                <input class="form-control" type="text" id="TipoVialidad" value="<?=$empleadoObj ? $empleadoObj->TipoVialidad : "" ?>" name="TipoVialidad" />
            </div>
            <div class="form-group">
                <label>NombreVialidad*</label>
                <input class="form-control" type="text" id="NombreVialidad" value="<?=$empleadoObj ? $empleadoObj->NombreVialidad : "" ?>" name="NombreVialidad" />
            </div>
            <div class="form-group">
                <label>NumeroExterior*</label>
                <input class="form-control" type="text" id="NumeroExterior" value="<?=$empleadoObj ? $empleadoObj->NumeroExterior : "" ?>" name="NumeroExterior" />
            </div>
            <div class="form-group">
                <label>NumeroInterior</label>
                <input class="form-control" type="text" id="NumeroInterior" value="<?=$empleadoObj ? $empleadoObj->NumeroInterior : "" ?>" name="NumeroInterior" />
            </div>
        </div>
    </div>
    <div class="row">
        <legend>Estudios</legend>
        <div class="col-md-7">

            <table class="table" id='estudios'> 
                <thead>
                    <tr>
                        <th>Escuela</th>
                        <th>Grado Estudio</th>
                        <th>FechaInicio</th>
                        <th>FechaFin</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if($empleadoObj && isset($empleadoObj->Estudios)):?>
                        <?php foreach($empleadoObj->Estudios as $o): ?>
                        <tr><td><?=$o->Escuela?></td><td><?=$o->Grado?></td><td><?=$o->FechaInicio?></td><td><?=$o->FechaFin?></td><td><button class="btn btn-danger eliminar" data-id="0">X</button></td></tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    </tbody>
            </table>
        </div>
        <div class="col-md-5">
            <div class="form-group">
                <label>Escuela*</label>
                <input class="form-control" type="text" id="Escuela" name="Escuela" />
            </div>
            <div class="form-group">
                <label>Grado Estudio*</label>
                <select class="form-control set_select2" id="Grado" name="Grado">
                    <?php foreach($grados as $o): ?>
                    <option value="<?=$o["Id"] ?>">
                        <?=$o["Descripcion"]?>
                    </option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label>FechaInicio*</label>
                <input class="form-control" type="date" id="FechaInicio" name="FechaInicio" />
            </div>
            <div class="form-group">
                <label>FechaFin*</label>
                <input class="form-control" type="date" id="FechaFin" name="FechaFin" />
            </div>
            <div class="form-group">
               <button class="btn btn-primary pull-right" id='agregar'>Agregar</button>
            </div>
        </div>
    </div>
    <hr> 
    <button class="btn btn-primary pull-right" id='guardar'>Guardar</button>
    <a href="?" class="btn btn-danger pull-right" >Regresar</button>
    <script src="js/create.js"></script>