<div class="container">
<div class="row">
        <div class="col-md-12">
            <a href="?page=crear" class="btn btn-primary pull-right">Nuevo</a>
        </div>
    </div>
</div>
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Apellido Paterno</th>
                        <th>Apellido Materno</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($empleados as $o): ?>
                    <tr>
                        <td><?=$o->Nombre?></td>
                        <td><?=$o->ApellidoPaterno?></td>
                        <td><?=$o->ApellidoMaterno?></td>
                        <td><a class="btn btn-primary" href="?page=crear&Id=<?=$o->Id?>">Ver</a></td>
                    </tr>
                    
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>